<?php

$sort = 'recency';
$url = 'https://www.upwork.com/ab/feed/jobs/rss?subcategory2=web_development&paging=0%3B50&sort='.$sort.'&api_params=1&q=&securityToken=8f694b7779b56b483de7ab1f14eeb71a77db4bb46cce100d8e22d3a6969b5e063b4f01c98cada6c262db262ef6395fe3abe51baecee8d4e9940b5b23852bfaad&userUid=1004454894076182528&orgUid=1004454894105542657';

$rss = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA);

$pathToStorageFile = 'added.txt';

$sendQueue = [];
foreach ($rss->channel->item as $item) {
    $itemId = (string)$item->guid;
    if (isAdded($itemId, $pathToStorageFile)) {
        break;
    }
    prependToFile($itemId, $pathToStorageFile);
    array_unshift($sendQueue, $item);
}

if (!empty($sendQueue)) {
    $apiToken = '884087666:AAFRLAzzxdri3Zz1H_I5-02LxKllbFJajHY';
    $chat_id = '-1001441400818';
    $parse_mode = 'html'; // or markdown

    foreach ($sendQueue as $item) {
        $text = format($item);
        $data = [
            'parse_mode' => $parse_mode,
            'chat_id' => $chat_id,
            'text' => $text,
        ];
        httpPost('https://api.telegram.org/bot'. $apiToken .'/sendMessage', $data);
    }
}

function httpPost($url, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function format($item) {
    $title = str_replace(' - Upwork', '', (string)$item->title);
    $desc = str_replace('<br />', PHP_EOL, (string)$item->description);

    $text = '<b>Title: </b><a href="'.(string)$item->link.'">'.$title.'</a>'
        . PHP_EOL . PHP_EOL . '<b>Description: </b>'. $desc;

    return $text;
}

function prependToFile($itemId, $pathToStorageFile) {
    $file_to_read = @fopen($pathToStorageFile, "r");
    $old_text = @fread($file_to_read, 5120);
    @fclose($file_to_read);
    $file_to_write = fopen($pathToStorageFile, "w");
    fwrite($file_to_write, $itemId . PHP_EOL .$old_text);
}

function isAdded($itemId, $pathToStorageFile) {
    $handle = fopen($pathToStorageFile, 'a+');
    while (($buffer = fgets($handle)) !== false) {
        if (strpos($buffer, $itemId) !== false) {
            fclose($handle);
            return true;
        }
    }

    return false;
}
